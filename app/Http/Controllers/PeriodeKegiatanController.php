<?php

namespace App\Http\Controllers;

use App\Models\PeriodeKegiatan;
use Illuminate\Http\Request;

class PeriodeKegiatanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.periode.list');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.periode.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PeriodeKegiatan  $periodeKegiatan
     * @return \Illuminate\Http\Response
     */
    public function show(PeriodeKegiatan $periodeKegiatan)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\PeriodeKegiatan  $periodeKegiatan
     * @return \Illuminate\Http\Response
     */
    public function edit(PeriodeKegiatan $periodeKegiatan)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\PeriodeKegiatan  $periodeKegiatan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PeriodeKegiatan $periodeKegiatan)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PeriodeKegiatan  $periodeKegiatan
     * @return \Illuminate\Http\Response
     */
    public function destroy(PeriodeKegiatan $periodeKegiatan)
    {
        //
    }
}
