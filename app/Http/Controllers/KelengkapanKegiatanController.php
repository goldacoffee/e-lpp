<?php

namespace App\Http\Controllers;

use App\Models\KelengkapanKegiatan;
use Illuminate\Http\Request;

class KelengkapanKegiatanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\KelengkapanKegiatan  $kelengkapanKegiatan
     * @return \Illuminate\Http\Response
     */
    public function show(KelengkapanKegiatan $kelengkapanKegiatan)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\KelengkapanKegiatan  $kelengkapanKegiatan
     * @return \Illuminate\Http\Response
     */
    public function edit(KelengkapanKegiatan $kelengkapanKegiatan)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\KelengkapanKegiatan  $kelengkapanKegiatan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, KelengkapanKegiatan $kelengkapanKegiatan)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\KelengkapanKegiatan  $kelengkapanKegiatan
     * @return \Illuminate\Http\Response
     */
    public function destroy(KelengkapanKegiatan $kelengkapanKegiatan)
    {
        //
    }
}
