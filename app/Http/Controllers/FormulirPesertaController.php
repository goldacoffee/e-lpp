<?php

namespace App\Http\Controllers;

use App\Models\FormulirPeserta;
use Illuminate\Http\Request;

class FormulirPesertaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\FormulirPeserta  $formulirPeserta
     * @return \Illuminate\Http\Response
     */
    public function show(FormulirPeserta $formulirPeserta)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\FormulirPeserta  $formulirPeserta
     * @return \Illuminate\Http\Response
     */
    public function edit(FormulirPeserta $formulirPeserta)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\FormulirPeserta  $formulirPeserta
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, FormulirPeserta $formulirPeserta)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\FormulirPeserta  $formulirPeserta
     * @return \Illuminate\Http\Response
     */
    public function destroy(FormulirPeserta $formulirPeserta)
    {
        //
    }
}
