<?php

namespace App\Http\Controllers;

use App\Models\KelengkapanPeserta;
use Illuminate\Http\Request;

class KelengkapanPesertaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\KelengkapanPeserta  $kelengkapanPeserta
     * @return \Illuminate\Http\Response
     */
    public function show(KelengkapanPeserta $kelengkapanPeserta)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\KelengkapanPeserta  $kelengkapanPeserta
     * @return \Illuminate\Http\Response
     */
    public function edit(KelengkapanPeserta $kelengkapanPeserta)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\KelengkapanPeserta  $kelengkapanPeserta
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, KelengkapanPeserta $kelengkapanPeserta)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\KelengkapanPeserta  $kelengkapanPeserta
     * @return \Illuminate\Http\Response
     */
    public function destroy(KelengkapanPeserta $kelengkapanPeserta)
    {
        //
    }
}
