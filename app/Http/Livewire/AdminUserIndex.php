<?php

namespace App\Http\Livewire;

use App\Models\User;
use Livewire\Component;

class AdminUserIndex extends Component
{
    public $userList;

    public function mount()
    {
        $this->userList = $this->getUserList();
    }

    public function getUserList()
    {
        $user = User::get();
        return $user;
    }
    public function render()
    {
        return view('livewire.admin-user-index');
    }
}
