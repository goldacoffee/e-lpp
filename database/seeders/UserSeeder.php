<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Ramsey\Uuid\Uuid;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**
         * Admin User
         *
         */

        User::updateOrCreate(['email' => 'admin@lpp.com'], [
            'id' => Str::uuid()->toString(),
            'name' => 'admin',
            'password' => Hash::make('adminpass2020'),
            'level' => 'admin',

        ]);

        /**
         * Staff User
         */
        User::updateOrCreate(['email' => 'staff@lpp.com'], [
            'id' => Str::uuid()->toString(),
            'name' => 'staff',
            'password' => Hash::make('staffpass2020'),
            'level' => 'staff',

        ]);

        /**
         * User Biasa
         */

        User::updateOrCreate(['email' => 'user@lpp.com'], [
            'id' => Str::uuid()->toString(),
            'name' => 'user',
            'password' => Hash::make('userpass2020'),
            'level' => 'user',

        ]);
    }
}
