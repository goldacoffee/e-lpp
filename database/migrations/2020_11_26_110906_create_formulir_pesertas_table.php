<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFormulirPesertasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('formulir_peserta', function (Blueprint $table) {
            $table->uuid('id');
            $table->string('nama_lengkap');
            $table->string('gelar_akademis', 10);
            $table->string('jenis_kelamin', 15);
            $table->string('no_nik', 20);
            $table->string('status_kepegawaian', '10'); // CPNS,PNS,POLRI/PTT/SWASTA
            $table->string('nomor_status_kepegawaian');
            $table->string('pendidikan_terakhir', 10); //S1,D1,D2,D3,S1,S2,S3
            $table->string('nama_instansi');
            $table->string('alamat_kantor');
            $table->string('kode_pos');
            $table->string('paket', 60);
            $table->string('pembayaran', 60);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('formulir_peserta');
    }
}
