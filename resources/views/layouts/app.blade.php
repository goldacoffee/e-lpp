@extends('layouts.dashboard')

@section('app')
<div class="main-wrapper">
    <div class="navbar-bg"></div>
    <nav class="navbar navbar-expand-lg main-navbar">
        @include('partials.topnav')
    </nav>
    <div class="main-sidebar">
        @include('partials.sidebar')
    </div>

    <!-- Main Content -->
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>@yield('h1')</h1>
                <div class="section-header-breadcrumb">
                    {{-- <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
                    <div class="breadcrumb-item"><a href="#">Bootstrap Components</a></div>
                    <div class="breadcrumb-item">Alert</div> --}}
                    @yield('nav-breadcumb')
                </div>
            </div>
        </section>
        <div class="section-body">

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            @yield('card-header','')
                        </div>
                        <div class="card-body">
                            @yield('content')

                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <footer class="main-footer">
        @include('partials.footer')
    </footer>
</div>
@endsection
