@extends('admin.dashboard')

@section('h1',__('title.h1.admin.user.add'))
@section('title',__('title.h1.admin.user.add'))
@section('card-header','')

@section('content')
@livewire('admin-user-add', )
@endsection
