@extends('admin.dashboard')

@section('h1',__('title.h1.user.index'))
@section('title',__('title.h1.admin.index'))
@section('card-header','')

@section('content')
@livewire('admin-user-index')
@endsection


@push('javascript')
<script>
    $("#user-list").DataTable({ order: [] });
</script>

@endpush
