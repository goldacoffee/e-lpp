@extends('layouts.auth')
@section('content')

<div class="card card-primary">
    <div class="card-body">
        @if ($errors->has('failed'))
        <div class="alert alert-primary alert-dismissible show fade">
            <div class="alert-body">
                <button class="close" data-dismiss="alert">
                    <span>&times;</span>
                </button>
                {{ $errors->first('failed') }}
            </div>
        </div>

        @endif

        <form action="{{ route('auth') }}" method="POST">
            @csrf
            <div class="form-group">
                <label for="email" class="text-center">Email</label>
                <input type="text" class="form-control {{ ($errors->has('email')) ? 'is-invalid': ''  }}" name="email"
                    id="email" value="{{ old('email') }}">
                <small class="text-danger">
                    {{ ($errors->has('email')) ? $errors->first('email'): ''  }}
                </small>
            </div>
            <div class="form-group">
                <label for="password" class="text-center">Password</label>
                <input type="password" class="form-control {{ ($errors->has('password')) ? 'is-invalid': ''  }}"
                    name="password" id="password" value="{{ old('password') }}">
                <small class="text-danger">
                    {{ ($errors->has('password')) ? $errors->first('password'): ''  }}
                </small>
            </div>
            <div class="form-group">
                <input type="submit" value="LOGIN" class="btn btn-primary  btn-block">
            </div>
        </form>
    </div>
</div>

@endsection
