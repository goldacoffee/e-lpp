@if (Auth::user())

@includeIf('admin.dashboard')
@else
@includeIf('admin.auth')

@endif
