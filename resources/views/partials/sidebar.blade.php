<aside id="sidebar-wrapper">


    <div class="sidebar-brand">
        <a href="">{{ config('app.name') }}</a>
    </div>
    <div class="sidebar-brand sidebar-brand-sm">
        <a href="#">{{ strtoupper(substr(config('app.name'), 0, 2)) }}</a>
    </div>
    <ul class="sidebar-menu">
        {{-- @foreach (Helpers::listMenu() as $key=> $item)
        @if (count($item['sub'])>0)

        @else

        <li class="{{ Route::currentRouteName() === $item['route'] ? 'active' : '' }}">
        <a class="nav-link" href="{{ route($item['route']) }}">
            <i class="{{ $item['icon'] }}"></i> <span>{{ $item['title'] }}</span>
        </a>
        </li>
        @endif

        @endforeach --}}

        @foreach (Helpers::listMenu() as $item)
        @isset($item['sub'])
        <li class="dropdown {{ (Str::contains(Route::currentRouteName(),$item['param']))  ? 'active' : ''  }}">
            <a href="#" class="nav-link has-dropdown"><i class="fas fa-th"></i> <span>{{ $item['title'] }}</span></a>
            <ul class="dropdown-menu">
                @foreach ($item['sub'] as $itemSub)
                @isset($itemSub['param'])
                <li
                    class="{{ Route::currentRouteName() === $itemSub['route'] ? 'active' : '' }} {{ (Str::contains(Route::currentRouteName(),$itemSub['param']))  ? 'active' : ''  }}">
                    <a class="nav-link " href="{{ route($itemSub['route']) }}">
                        <i class="{{ $itemSub['icon'] }}"></i> <span>{{ $itemSub['title'] }}</span>
                    </a>
                </li>
                @else
                <li class="{{ Route::currentRouteName() === $itemSub['route'] ? 'active' : '' }}">
                    <a class="nav-link " href="{{ route($itemSub['route']) }}"><i class="{{ $itemSub['icon'] }}"></i>
                        <span>{{ $itemSub['title'] }}</span></a></li>
                @endisset
                @endforeach
            </ul>
        </li>


        @else
        <li class="{{ Route::currentRouteName() === $item['route'] ? 'active' : '' }}">
            <a class="nav-link" href="{{ route($item['route']) }}">
                <i class="{{ $item['icon'] }}"></i> <span>{{ $item['title'] }}</span>
            </a>
        </li>
        @endisset
        @endforeach






    </ul>


</aside>
