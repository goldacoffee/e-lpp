<div>
    {{-- A good traveler has no fixed plans and is not intent upon arriving. --}}
    @isset($userList)

    <div class="table-responsive">
        <table class="table table-striped table-sm" id="user-list">
            <thead>
                <tr>
                    <th>Nama</th>
                    <th>Email</th>
                    <th>HP</th>
                    <th>Level</th>
                    <th class="text-right"></th>
                </tr>
            </thead>
            <tbody>


                @foreach ($userList as $key=> $item)
                <tr>
                    <td>{{ $item->name }}</td>
                    <td>{{ $item->email }}</td>
                    <td>{{ $item->phone }}</td>
                    <td>{{ $item->level }}</td>
                    <td class="text-right">

                        <button class="btn btn-primary btn-sm">Edit</button>
                        <button class="btn btn-info btn-sm ">Reset Pass</button>
                        <button class="btn btn-danger btn-sm">Delete</button>

                    </td>
                </tr>

                @endforeach
            </tbody>
        </table>




        @endisset
    </div>
