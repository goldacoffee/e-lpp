<?php

/**
 * Template:
 * "admin.user."=>"",
 */
return [
    "admin.user.h2" => "Tambah Pengguna Baru",
    "admin.user.name" => "Nama Lengkap",
    "admin.user.email" => "Alamat Email",
    "admin.user.phone" => "Nomor HP",
    "admin.user.level" => "Hak Akses",
    'admin.user.password' => 'Kata Sandi',
    'admin.user.password.confirm' => 'Konfirmasi Kata Sandi',
    'admin.user.button.submit' => 'Tambah Pengguna',

];
